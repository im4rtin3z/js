const numbers = [1, 2, 3];

[num1, , num3] = numbers;

console.log(num1);
console.log(num3);

const {name} = {name:"max", age:28};

console.log(name);
//console.log(age); it will generate error, because is not defined
