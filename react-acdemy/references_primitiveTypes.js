const number = 1; // primitive type
const person = {
    name: "max"
};
const newPerson = person; // Reference
const newSecondPerson = {...person}; // Copied by spread operator
person.name = "ivan";

console.log(newPerson); //Produces ivan because the object is a reference
console.log(newSecondPerson); 
