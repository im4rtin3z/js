const numbers = [1, 2, 3];
//spread operator
const newNumbers = [...numbers, 4];
console.log(newNumbers);

const person = {
    name: 'ivan',
    lastName: 'Martinez'
};
const newPerson = { ...person, age: 28 };
console.log(newPerson);


//rest operator
const filter = (...args) => {
    return args.filter(element => element === 1);
}
console.log(filter(1, 2, 3, 4));

